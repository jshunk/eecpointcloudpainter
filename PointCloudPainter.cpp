#include "PointCloudPainter.h"
#include "Messenger/Messenger.h"
#include "Windows Input/MouseMessage.h"

CPointCloudPainter::CPointCloudPainter( CColor oColor )
	: m_oPointCloud( oColor )
{
	CMessenger::GlobalListen( *this, CMouseMessage::GetMouseMessageUID() );
}

CPointCloudPainter::~CPointCloudPainter()
{
	CMessenger::GlobalStopListening( *this, CMouseMessage::GetMouseMessageUID() );
}

void CPointCloudPainter::Push( CMessage& rMessage )
{
	CMouseMessage& rMouseMessage( static_cast< CMouseMessage& >( rMessage ) );
	if (rMouseMessage.GetMouseAction() == CMouseMessage::LEFT_BUTTON_CLICK)
	{
		m_oPointCloud.Add( rMouseMessage.X(), rMouseMessage.Y() );
	}
}
