#pragma once

#include "Color/Color.h"
#include "Messenger/Listener.h"
#include "OpenGL 2D Point Cloud/OpenGL2DPointCloud.h"

class CWindow;

class CPointCloudPainter : public CListener
{
public:
							CPointCloudPainter( CColor oColor );
							~CPointCloudPainter() override;
	void					Push( CMessage& ) override;
	COpenGL2DPointCloud&	GetPointCloud() { return m_oPointCloud; }

private:
	COpenGL2DPointCloud	m_oPointCloud;
};
